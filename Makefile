CXX = g++
CXXFLAGS = -g -Wall -Wextra

.PHONY: all
all : program

program: main.cpp Event.o
	$(CXX) $(CXXFLAGS) -o $@ $^
Event.o: Event.cpp Event.h
	$(CXX) $(CXXFLAGS) -c $<
.PHONY: clean
clean:
	rm *~ *.o program.exe
