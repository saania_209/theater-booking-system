#ifndef _EVENT_H_
#define _EVENT_H_

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
//trying to add derived classes
#include <iomanip>
using namespace std;

class event_booking
{

public:
    void first();
    void show_seat_frm_file();
    void show();
    void seatdisp(int z,int wt);
    void cancelseatdisp(int z,int wt);
    void book(int z,int wt ,int a);
    void seatin(int z,int wt);
    void cancel(int z,int wt ,int a);
    void manager();
    void ticket_print();
    void pass_proctect();
    void data_str();
    int password_check();
    void hal_name();
};
#endif //Event_h
